# Mega Build for Coman Orocos Interface

## Add your ssh keys to GitLab FIRST

Navigate to **User Settings** > **SSH Keys**

## Install Cogimon Toolkit

Follow the steps in this tutorial

```
http://cogimon.github.io/runtime/gettingstarted.html
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
echo "source ~/citk/systems/cogimon-core-ros-nightly/bin/setup-cogimon-env.sh" >> ~/.bashrc
source ~/.bashrc
```

Follow steps from section 3: if you want to run simulations, from 2: if you want to run the coman through orocos

## Section 1: Cloning the Repository  

```
cd
git clone https://gitlab.com/Coman-Packages/coman-orocos-pkg.git
```

## Section 2: Building Dependencies for running Orocos on the Robot

```
cd ~/coman-orocos-pkg/iit-rtt-coman/yaml-cpp
mkdir build && cd build
cmake ../ && make -j8
mkdir -p ~/coman-orocos-pkg/coman_shared/build && cd ~/coman-orocos-pkg/coman_shared/build
cmake ../ && make -j8
cd ../../iit-rtt-coman/microstrain
mkdir build && cd build
cmake ../ && make -j8
cd ../../rtt_coman
mkdir build && cd build
cmake ../ && make -j8
```

## Section 3: Building Walking Code

**Remember to check CMakeLists.txt in walking-rtt to make RealRobot Build or SimRobot Build**

Change ops file urdf and srdf load paths by replacing /home/biorob/ with /home/user/ in walking-rtt/Walking/ops/  

path to ops file :

`gedit ~/coman-orocos-pkg/walking-rtt/Walking/ops/TestComanWalking.ops`

Look for the urdf_path and srdf_path and change the paths by adding your current username to it in place of /home/biorob/

## Follow the following commands:  

## IF SIMULATION : 

Ctrl + Shift + T (**In a new Terminal**)  

`roscore`

Ctrl + Shift + T (**In a new Terminal**)

```
cd ~/coman-orocos-pkg
mkdir build && cd build
cmake ../walking-rtt/
source ../walking-rtt/setup-cogimon-walkingrtt-env.sh
deployer-gnulinux
```

## SimRobot (Replace $HOME with /home/user/)

`Deployer [S]> loadService("this", "scripting")`

`Deployer [S]> scripting.runScript("$HOME/coman-orocos-pkg/walking-rtt/Walking/ops/TestComanWalking.ops")`

## RealRobot (Replace $HOME with /home/user/)

`Deployer [S]> loadService("this", "scripting")`

`Deployer [S]> scripting.runScript("$HOME/coman-orocos-pkg/walking-rtt/Walking/ops/coman_walking_real.ops")`

Ctrl + Shift + T (In a new Terminal) 

`gzclient`